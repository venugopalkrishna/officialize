var myApp = {
  mainMessage : "Welcome!",

  mainGreeting(){
    console.log("Welcome to LEDC!");
  }
}

myApp.module1 = {
  saySomething(message)
  {
    console.log(myApp.mainMessage, message, "Module 2");
  },

  doSomething(){
    console.log('module1 doSomething function');
  }
}

myApp.module1 = {
  saySomething(message)
  {
    console.log(myApp.mainMessage, message, "Second file here");
  },

  doSomething(){
    console.log('module1 doSomething function');
  }
}

(() =>{
  myApp.mainGreeting();

  myApp.module1.saySomething("LEDC files");

  function myFunction()
  {
    // var theHeading = doument.querySelector('h1');

    // theHeading.textContent = myApp.mainMessage;
  }

  myFunction();
});
