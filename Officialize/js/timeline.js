(function() {
	"use strict";

  var year2010txt = document.querySelector("#year2010txt"),
      year2010 = document.querySelectorAll(".year2010"),
      year2011txt = document.querySelector("#year2011txt"),
      year2011 = document.querySelectorAll(".year2011"),
      year2014txt = document.querySelector("#year2014txt"),
      year2014 = document.querySelectorAll(".year2014"),
      year2015txt = document.querySelector("#year2015txt"),
      year2015 = document.querySelectorAll(".year2015"),
      year2016txt = document.querySelector("#year2016txt"),
      year2016 = document.querySelectorAll(".year2016"),
      year2017txt = document.querySelector("#year2017txt"),
      year2017 = document.querySelectorAll(".year2017"),
      txtTime = document.querySelectorAll(".text"),
      Timeimg = document.querySelectorAll(".image");

      function Show2010txt() {
        year2010txt.style.opacity = 1;
        year2011txt.style.opacity = 0;
        year2014txt.style.opacity = 0;
        year2015txt.style.opacity = 0;
        year2016txt.style.opacity = 0;
        year2017txt.style.opacity = 0;
      }

      function Show2011txt() {
        year2010txt.style.opacity = 0;
        year2011txt.style.opacity = 1;
        year2014txt.style.opacity = 0;
        year2015txt.style.opacity = 0;
        year2016txt.style.opacity = 0;
        year2017txt.style.opacity = 0;
      }

      function Show2014txt() {
        year2010txt.style.opacity = 0;
        year2011txt.style.opacity = 0;
        year2014txt.style.opacity = 1;
        year2015txt.style.opacity = 0;
        year2016txt.style.opacity = 0;
        year2017txt.style.opacity = 0;
      }

      function Show2015txt() {
        year2010txt.style.opacity = 0;
        year2011txt.style.opacity = 0;
        year2014txt.style.opacity = 0;
        year2015txt.style.opacity = 1;
        year2016txt.style.opacity = 0;
        year2017txt.style.opacity = 0;
      }

      function Show2016txt() {
        year2010txt.style.opacity = 0;
        year2011txt.style.opacity = 0;
        year2014txt.style.opacity = 0;
        year2015txt.style.opacity = 0;
        year2016txt.style.opacity = 1;
        year2017txt.style.opacity = 0;
      }

      function Show2017txt() {
        year2010txt.style.opacity = 0;
        year2011txt.style.opacity = 0;
        year2014txt.style.opacity = 0;
        year2015txt.style.opacity = 0;
        year2016txt.style.opacity = 0;
        year2017txt.style.opacity = 1;
      }

      for (var i=0;i<year2010.length;i+=1) {
        year2010[i].addEventListener("click", Show2010txt, false);}
      for (var i=0;i<year2011.length;i+=1) {
        year2011[i].addEventListener("click", Show2011txt, false);}
      for (var i=0;i<year2014.length;i+=1) {
        year2014[i].addEventListener("click", Show2014txt, false);}
      for (var i=0;i<year2015.length;i+=1) {
        year2015[i].addEventListener("click", Show2015txt, false);}
      for (var i=0;i<year2016.length;i+=1) {
        year2016[i].addEventListener("click", Show2016txt, false);}
      for (var i=0;i<year2017.length;i+=1) {
        year2017[i].addEventListener("click", Show2017txt, false);}

})();
