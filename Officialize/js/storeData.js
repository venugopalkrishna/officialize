var storeData = {
  // add info for the cars here
  men28: {

  			merchN : "MEN'S 28 STRIKES AGAIN",

        price: "$19.99",

  			images : "mg-product-1-large.png",

        li1 : "T-shirt",
        li2 : "Blue",
        li3 : "100% soft spun cotton, 30 singles"
  		},

      mendreads: {

      			merchN : "MEN'S NATIVE DREADS",

            price: "$29.99",

      			images : "mg-product-8-large.png",

            li1 : "T-shirt",
            li2 : "Red",
            li3 : "100% soft spun cotton, 30 singles"
  		},

      menFlash: {

      			merchN : "MEN'S FLASH 28",

            price: "$29.99",

      			images : "mg-product-3-large.png",

            li1 : "100% Ring spun cotton",
            li2 : "Mens Short Sleeve Navy Crew Neck Tee",
            li3 : "20/1 Fine Knit Jersey"
  		}
  	};
