(function() {
	"use strict";

	var filterPop = document.querySelector(".filterInf");
	var filter = document.querySelector(".filter");
	var sizelist = document.querySelector(".sizes");
	var size = document.querySelectorAll(".size");
	var sizeName = ["S","M","L","XL","2XL"];
	var actPop = document.querySelectorAll(".product");
	var titlePro = document.querySelector(".title");
	var imgDis = document.querySelector(".imgDis");
	var price = document.querySelector(".price");
	var li1 = document.querySelector(".li1");
	var li2 = document.querySelector(".li2");
	var li3 = document.querySelector(".li3");
	var men28 = document.querySelectorAll(".men-28");
	var popM28 = document.querySelector("#popUp-men28");
	var popUp = document.querySelectorAll(".prodInf");
	var closePU = document.querySelectorAll(".closePU");

// functions
function showFilter() {
	filterPop.style.display = "block";
}

function sizeSelected() {
	var theSelected = sizeName.indexOf(String(event.target.innerHTML));
	size[theSelected].style.backgroundColor = "#186bbc";
}

function showProduct(event) {
	event.preventDefault();

	popM28.style.display = "block";

	let objectIndex = storeData[this.id];

	titlePro.firstChild.nodeValue = objectIndex.merchN;
  price.firstChild.nodeValue = objectIndex.price;
	imgDis.src = "images/storeDetails/" + objectIndex.images;
	li1.firstChild.nodeValue = objectIndex.li1;
	li2.childNodes[0].nodeValue = objectIndex.li2;
	li3.lastChild.nodeValue = objectIndex.li3;
}

function close(event) {
	event.preventDefault();
  popM28.style.display = "none";
	filterPop.style.display = "none";
}

for (var i=0;i<men28.length;i+=1) {
  men28[i].addEventListener("click", showProduct, false);
}
for(var i=0; i<closePU.length; i++) {
  closePU[i].addEventListener("click", close, false);
}
for (var i=0; i<size.length; i++){
size[i].addEventListener('click', sizeSelected, false);
}

filter.addEventListener('click', showFilter, false);


})();
