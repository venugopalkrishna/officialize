(function(){
"use strict"

var bioDesk = document.querySelector("#bioDesk");
var headings = document.querySelectorAll(".heading");
var h1text = document.querySelectorAll(".h1tag");
// headings
var hcol = document.querySelector("#college");
var hcar = document.querySelector("#career");
var hper = document.querySelector("#personal");
var imgH = document.querySelectorAll(".disImg");
// descriptions
var Dcol = document.querySelector("#descCol");
var Dcar = document.querySelector("#descCar");
var Dper = document.querySelector("#descPer");

var closeBio = document.querySelectorAll(".closeBio");

function closeDeskBio() {
  console.log("lol");
  Dcol.style.display = "none";
  Dcar.style.display = "none";
  Dper.style.display = "none";

  for (var i=0;i<h1text.length;i+=1) {
  h1text[i].style.transform = "rotate(0deg)";
  h1text[i].style.float = "none";
  h1text[i].style.display = "none";
}

for (var i=0;i<headings.length;i+=1) {
  headings[i].classList.remove("col-lg-2", "col-md-2");
  headings[i].classList.add("col-lg-4", "col-md-4");
  headings[i].style.borderLeft = "none";
}
hper.style.background = "#052956";
hcol.style.background = "#052956";
hcar.style.background = "#052956";


// for (var i=0;i<headings.length;i+=1) {
// headings[i].classList.remove("col-lg-4", "col-md-4");
// }
for (var i=0;i<imgH.length;i+=1) {
imgH[i].style.display = "block";
}
hcol.style.borderLeft = "none";
bioDesk.style.border = "none";

}

for (var i=0; i<closeBio.length; i++){
closeBio[i].addEventListener('click', closeDeskBio, false);
}

function ShowCollegeDescription () {
  // headings.style.transform = "rotate(90deg)";
  // headings.style.float = "left";
  // description show
  Dcol.style.display = "block";
  Dcar.style.display = "none";
  Dper.style.display = "none";
  for (var i=0;i<h1text.length;i+=1) {
  h1text[i].style.transform = "rotate(-90deg)";
  h1text[i].style.float = "left";
  // h1text[i].classList.add("vertical-center");
  // h1text[i].style.position = "none";
  h1text[i].style.display = "block";
}
// for (var i=0;i<h1text.length;i+=1) {
// h1text[i].style.float = "left";
// }

hcol.style.background = "#093e7d";
hper.style.background = "#052956";
hcar.style.background = "#052956";

for (var i=0;i<headings.length;i+=1) {
headings[i].classList.add("col-lg-2", "col-md-2");
headings[i].classList.remove("col-lg-4", "col-md-4");
headings[i].style.borderLeft = "15px #000917 solid";
headings[i].style.padding = "none";
// headings[i].style.border = "6px red";
}
// for (var i=0;i<headings.length;i+=1) {
// headings[i].classList.remove("col-lg-4", "col-md-4");
// }
for (var i=0;i<imgH.length;i+=1) {
imgH[i].style.display = "none";
}
hcol.style.borderLeft = "none";
bioDesk.style.border = "15px #000917 solid";
}

hcol.addEventListener('click', ShowCollegeDescription, false);

function ShowCareerDescription() {
  // headings.style.transform = "rotate(90deg)";
  // headings.style.float = "left";
  // description show
  Dcol.style.display = "none";
  Dcar.style.display = "block";
  Dper.style.display = "none";



  for (var i=0;i<h1text.length;i+=1) {
  h1text[i].style.transform = "rotate(-90deg)";
  h1text[i].style.float = "left";
  h1text[i].style.display = "block";
  // h1text[i].classList.add("vertical-center", "whenClick");
  // h1text[i].classList.remove("h1tag");
}

hcar.style.background = "#093e7d";
hper.style.background = "#052956";
hcol.style.background = "#052956";


// for (var i=0;i<h1text.length;i+=1) {
// h1text[i].style.float = "left";
// }
for (var i=0;i<headings.length;i+=1) {
  headings[i].classList.add("col-lg-2", "col-md-2");
  headings[i].classList.remove("col-lg-4", "col-md-4");
  headings[i].style.borderLeft = "15px #000917 solid";
  headings[i].style.padding = "none";
}
// for (var i=0;i<headings.length;i+=1) {
// headings[i].classList.remove("col-lg-4", "col-md-4");
// }
for (var i=0;i<imgH.length;i+=1) {
imgH[i].style.display = "none";
}
hcol.style.borderLeft = "none";
bioDesk.style.border = "15px #000917 solid";
}

hcar.addEventListener('click', ShowCareerDescription, false);

function ShowPersonalDescription() {
  // headings.style.transform = "rotate(90deg)";
  // headings.style.float = "left";
  // description show
  Dcol.style.display = "none";
  Dcar.style.display = "none";
  Dper.style.display = "block";

  for (var i=0;i<h1text.length;i+=1) {
  h1text[i].style.transform = "rotate(-90deg)";
  h1text[i].style.float = "left";
  // h1text[i].classList.add("vertical-center");
  h1text[i].style.display = "block";
}
// for (var i=0;i<h1text.length;i+=1) {
// h1text[i].style.float = "left";
// }
for (var i=0;i<headings.length;i+=1) {
  headings[i].classList.add("col-lg-2", "col-md-2");
  headings[i].classList.remove("col-lg-4", "col-md-4");
  headings[i].style.borderLeft = "15px #000917 solid";
  headings[i].style.padding = "none";
}
hper.style.background = "#093e7d";
hcol.style.background = "#052956";
hcar.style.background = "#052956";


// for (var i=0;i<headings.length;i+=1) {
// headings[i].classList.remove("col-lg-4", "col-md-4");
// }
for (var i=0;i<imgH.length;i+=1) {
imgH[i].style.display = "none";
}
hcol.style.borderLeft = "none";
bioDesk.style.border = "15px #000917 solid";
}

hper.addEventListener('click', ShowPersonalDescription, false);


})();
