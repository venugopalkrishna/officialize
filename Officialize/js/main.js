(() =>{
  myApp.mainGreeting();

  myApp.module1.saySomething("LEDC files");

  function myFunction()
  {
    // var theHeading = doument.querySelector('h1');

    // theHeading.textContent = myApp.mainMessage;
  }

  myFunction();
});

(function() {
	"use strict";

	var pages = document.querySelectorAll("#productsNav a");
	var productCons = document.querySelectorAll(".prodConTog");
	console.log(pages);

	function changeProdCon(evt) {
		console.log(evt);
		//grabbing info from data attribute
		//used data-but in html file
		console.log(evt.currentTarget.dataset.but);

		//hide all the divs
		for(var i=0; i<productCons.length; i++) {
			productCons[i].style.display = "none";
		}

		//need to show the div that was being clicked on
		var pageNum = evt.currentTarget.dataset.but;
		//feed variable into querySelector, makes section dynamic
		var content = document.querySelector("#"+pageNum);
		content.style.display = "block"; //makes div show when clicked

	}

	for(var i=0; i<pages.length; i++) {
		pages[i].addEventListener("click", changeProdCon, false);
		//switchDiv is the event handler
	}


})();
